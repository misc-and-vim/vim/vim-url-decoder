
function URL_decode(url)

	let l:res = {}
	let l:submatches = matchlist(a:url, '\v([^:]+):(.*)')
	let l:res['scheme'] = l:submatches[1]
	let l:url_to_parse = l:submatches[2]

	let l:submatches = matchlist(l:url_to_parse, '\v//([^:]+:[^@]+)\@(.*)')
	if len(l:submatches) != 0
		let l:user_password = split(l:submatches[1], ':')
		let l:res["userinfo"] = {}
		let l:res["userinfo"]["username"] = l:user_password[0]
		let l:res["userinfo"]["password"] = l:user_password[1]
		let l:url_to_parse = l:submatches[2]
	endif

	let l:submatches = matchlist(l:url_to_parse, '\v(([0-9a-zA-Z]+\.){0,}[a-z]+)(.*)')
	if len(l:submatches) == 0
		echo 'Seems like there is no domain in your url'
		return l:res
	endif
	let l:res['domain'] = l:submatches[1]
	let l:url_to_parse = l:submatches[3]

	let l:submatches = matchlist(l:url_to_parse, '\v:([0-9]+)(.*)')
	if len(l:submatches) != 0
		let l:res["port"] = str2nr(l:submatches[1])
		let l:url_to_parse = l:submatches[2]
	endif

	let l:submatches = matchlist(l:url_to_parse, '\v((/[a-z0-9A-Z]+){0,})/?(.*)')
	let l:res['path'] = l:submatches[1]
	let l:url_to_parse = l:submatches[3]

	let l:submatches = matchlist(l:url_to_parse, '\v((\?([^#]*))?(#(.*))?)?$')
	let l:query = l:submatches[3]
	let l:res['query'] = {}
	for l:name_value in split(l:query, '&')
		let l:name_value_list = split(l:name_value, '=')
		let l:res['query'][l:name_value_list[0]] = l:name_value_list[1]
	endfor

	let l:res['fragment'] = l:submatches[5]

	return l:res
	
endfunction

function s:url_decode_test(url, expected)
	let l:res = URL_decode(a:url)
	if type(l:res) != v:t_dict || l:res != a:expected
		echo 'Failure'
		echo 'expected : '
		echo a:expected
		echo 'got : '
		echo l:res
	endif
endfunction

function s:url_decode_tests()
	let l:url_test = 'http://www.gitlab.com/projects?truc=merde'
	let l:expected = {'fragment': '', 'scheme' : 'http', 'domain' : 'www.gitlab.com', 'path' : '/projects', 'query' : {'truc':'merde'} }
	call s:url_decode_test(l:url_test, l:expected)
	let l:url_test = 'http://www.gitlab.com/projects'
	let l:expected = {'fragment' : '', 'scheme' : 'http', 'domain' : 'www.gitlab.com', 'path' : '/projects', 'query' : {} }
	call s:url_decode_test(l:url_test, l:expected)
	let l:url_test = 'https://www.gitlab.com/projects/this/api?key=value&key2=value2'
	let l:expected = { 'fragment' : '', 'scheme' : 'https', 'domain' : 'www.gitlab.com', 'path' : '/projects/this/api', 'query' : {'key' : 'value', 'key2' : 'value2'} }
	call s:url_decode_test(l:url_test, l:expected)
endfunction

function Test()
	call s:url_decode_tests()
endfunction

